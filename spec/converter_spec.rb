require 'spec_helper'
require 'fakefs'

describe ASCIIConverter::Converter do
  
  describe ".encode" do
    
    before(:each) do
      File.open("tmp.txt", "w")         { |f| f.write("hello") }
      File.open("another_tmp.txt", "w") { |f| f.write(" world!") }
    end
    
    it "adds byte arrays per call" do
      array = []

      array = ASCIIConverter::Converter.encode(array, "tmp.txt")
      expect(array).to eq([[104, 101, 108, 108, 111]])

      array = ASCIIConverter::Converter.encode(array, "another_tmp.txt")
      expect(array).to eq([[104, 101, 108, 108, 111], [32, 119, 111, 114, 108, 100, 33]])
    end

  end

  describe ".decode" do
    
    it "writes the byte array to the file" do
      array = [[104, 101, 108, 108, 111], [32, 119, 111, 114, 108, 100, 33]]

      ASCIIConverter::Converter.decode(array, "output.txt")
      ouput = File.read("output.txt")
      expect(ouput).to eq("hello")

      ASCIIConverter::Converter.decode(array, "output.txt")
      ouput = File.read("output.txt")
      expect(ouput).to eq("hello world!")
    end

  end

end