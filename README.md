# ASCII Converter #

This ruby script takes multiple files with ASCII messages, and encodes them into bytes. After encoding all the messages, it prints each message individually to the ASCII output file.

## Running ##

EXAMPLE:

`./bin/ascii_converter -f /Users/user/file.txt,/Users/user/another_file.txt -o /Users/user/output.txt`

`-f FILES` - REQUIRED: A comma delimited list with full paths to each file

`-o OUTPUT` - REQUIRED: A full path to the output file