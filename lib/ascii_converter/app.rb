require 'optparse'
require 'ostruct'
require 'pp'
require 'byebug'

module ASCIIConverter
  class App

    attr_reader :options

    def initialize(arguments, stdin)
      @arguments = arguments
      @stdin     = stdin

      # Set defaults
      @options         = OpenStruct.new
      @options.verbose = false
      @options.quiet   = false
      @options.files   = []
    end

    # Parse options, check arguments, then process the command
    def execute
      if parsed_options? && arguments_valid?
        puts "Start at #{DateTime.now}\n\n" if @options.verbose
        t_0 = Time.now
        output_options if @options.verbose

        process_command

        t_1 = Time.now
        puts "Runtime : #{t_1 - t_0} seconds" if @options.verbose
        puts "\nFinished at #{DateTime.now}" if @options.verbose
      end

    end


    def parsed_options?

      # Specify options
      opts = OptionParser.new
      opts.on('-h', '--help',     'Displays help message')                                            { puts opts; exit 0 }
      opts.on('-V', '--verbose',  'Verbose output')                                                   { @options.verbose           = true }
      opts.on('-q', '--quiet',    'Output as little as possible, overrides verbose')                  { @options.quiet             = true }
      opts.on('-f', '--files FILES', 'Input Files (absolute path) please use "," to seperate them' \
                                     'Ex: /Users/someuser/text.txt,/Users/someuser/another.txt')      { |val| @options.files = val  }
      opts.on('-o', '--output OUPUT', 'Output File (absolute path)' \
                                      'Ex: /Users/someuser/text.txt')                                 { |val| @options.output_file = val  }

      opts.parse!(@arguments) rescue return false

      process_options
      true
    end

    # Performs post-parse processing on options
    def process_options
      @options.verbose = false if @options.quiet
      @files = @options.files.split(",")
    end

    def output_options
      puts "Options:\n"

      @options.marshal_dump.each do |name, val|
        puts "  #{name} = #{val}"
      end
    end

    # True if required arguments were provided
    def arguments_valid?
      return false unless @options.files
      return false unless @options.output_file
      true
    end

    # Assumptions: 
    # One message per file, the commandline takes a list of files/messages to be encoded and
    # decoded one message at a time to the output file
    # Per 1) Files only contain ASCII characters
    def process_command
      output_bytes = []
      @files.each do |filename|
        output_bytes = Converter.encode(output_bytes, filename)
      end
      while output_bytes.size > 0
        Converter.decode(output_bytes, @options.output_file)
      end
    end

  end
end
