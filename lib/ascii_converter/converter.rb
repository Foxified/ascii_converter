module ASCIIConverter
  class Converter

    def self.encode(output_bytes, filename)
      File.open(filename, "r:ASCII").each do |file|
        output_bytes << file.bytes.to_a
      end
      output_bytes
    end

    def self.decode(input_bytes, filename)
      input = input_bytes.shift
      File.open(filename, 'a:ASCII') do |file|
        file.write(input.map { |character| character.chr }.join )
      end
    end

  end
end